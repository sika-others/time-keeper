Time Keeper
===========

Tool for spend time management

* __Authors__: [Ondrej Sika](http://ondrejsika.com/c.html)
* __GitHub__: <http://github.com/ondrejsika/time-keeper>


Documentation
-------------

### Installation

```
git clone https://github.com/ondrejsika/time-keeper.git
cd time-keeper
virtualenv env
./env/bin/pip install -r requirements
./manage.py syncdb
./manage.py migrate
```

### Run

```
./manage.py runserver
```
