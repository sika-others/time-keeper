from django.contrib import admin

from .models import Activity, Profile, TimePart


admin.site.register(Activity)
admin.site.register(Profile)
admin.site.register(TimePart)