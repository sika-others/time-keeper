from django import forms

from .models import Activity, TimePart, Profile


class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ("name", "watchers")

class TimePartForm(forms.ModelForm):
    start_dt = forms.DateTimeField(label="start")
    stop_dt = forms.DateTimeField(label="stop", required=False)

    class Meta:
        model = TimePart
        fields = ("note", )

class LoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput(render_value=True))

class SettingsForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ("deleted", "user",)
