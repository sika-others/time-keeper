import time
import datetime


def time2str(time):
    h = time / 3600
    m = (time - 3600*h) / 60
    s = (time - 3600*h - 60*m)
    return "%s:%s:%s" % (h, m, s)

def time2datetime(t):
    return datetime.datetime.fromtimestamp(t)

def datetime2time(dt):
    return time.mktime(dt.timetuple())

def time2date(t):
    return datetime.date.fromtimestamp(t)

def date2time(dt):
    return time.mktime(dt.timetuple())