import datetime
import time

from django.db import models
from django.contrib.auth.models import User

from .helpers import time2str, time2datetime, datetime2time

class BaseManager(models.Manager):
    def all(self):
        return super(BaseManager, self).all().order_by("-pk")

    def active(self):
        return self.all().filter(deleted=False)

class BaseModel(models.Model):
    deleted = models.BooleanField(default=False)

    objects = BaseManager()

    class Meta:
        abstract = True

class Profile(BaseModel):
    user = models.OneToOneField(User)

    day_start = models.TimeField(default=datetime.time(0, 0, 0))
    day_start_int = property(lambda self: self.day_start.hour * 3600 + self.day_start.minute * 60 + self.day_start.second)

    def __unicode__(self):
        return u"%s" % self.user

class Activity(BaseModel):
    NEXT_ACTION_START = 1
    NEXT_ACTION_STOP = 2

    owner = models.ForeignKey(Profile, related_name="activity_owner")
    watchers = models.ManyToManyField(Profile, related_name="related_watchers", blank=True)

    name = models.CharField(max_length=64)

    def get_total(self):
        return sum((obj.time for obj in self.timepart_set.active()))
    total = property(get_total)

    def get_total_str(self):
        return time2str(self.get_total())
    total_str = property(get_total_str)

    def get_last_timepart(self):
        try:
            return self.timepart_set.active().order_by("-pk")[0]
        except IndexError:
            return None

    def get_next_action(self):
        last = self.get_last_timepart()
        if not last:
            return self.NEXT_ACTION_START
        else:
            if last.stop:
                return self.NEXT_ACTION_START
            else:
                return self.NEXT_ACTION_STOP

    def get_time_from_interval(self, interval_start, interval_stop):
        time_sum = 0
        qs = list(self.timepart_set.active().order_by("pk").filter(stop__gt=interval_start, start__lt=interval_stop))
        last = None
        if not qs:
            try:               last = self.timepart_set.active()[0]
            except IndexError: pass

        if not last:
            try:               last = self.timepart_set.active().order_by("pk").filter(pk__gt=qs[-1].pk)[0]
            except IndexError: last = None

        if last and last.stop == None and last.start < interval_stop:
            time_sum += last.time

        if not qs:
            return time_sum

        time_sum += sum((obj.time for obj in qs))

        if qs[0].start < interval_start < qs[0].stop:
            time_sum -= (interval_start - qs[0].start)
        if qs[-1].start < interval_stop < qs[-1].stop:
            time_sum -= (qs[-1].stop - interval_stop)

        return time_sum

    def get_today(self):
        today = int(datetime2time(datetime.date.today())) + self.owner.day_start_int
        return self.get_time_from_interval(today, time.time())
    today = property(get_today)
    today_str = property(lambda self: time2str(self.today))

    def get_last_day(self):
        first_day = int(datetime2time(datetime.date.today()-datetime.timedelta(days=1))) + self.owner.day_start_int
        last_day = first_day + 86400  # 86400 = day in seconds
        return self.get_time_from_interval(first_day, last_day)
    last_day = property(get_last_day)
    last_day_str = property(lambda self: time2str(self.last_day))

    def get_this_month(self):
        today = datetime.date.today()
        first_day = int(datetime2time(datetime.date(today.year, today.month, 1))) + self.owner.day_start_int
        return self.get_time_from_interval(first_day, time.time())
    this_month = property(get_this_month)
    this_month_str = property(lambda self: time2str(self.this_month))

    def get_last_month(self):
        today = datetime.datetime.today()
        this_month_first_day = datetime.date(day=1, month=today.month, year=today.year)
        last_month_last_day = this_month_first_day - datetime.timedelta(days=1)
        last_month_first_day = datetime.date(year=last_month_last_day.year, month=last_month_last_day.month, day=1)

        first_day = int(datetime2time(last_month_first_day)) + self.owner.day_start_int
        last_day = int(datetime2time(this_month_first_day)) + self.owner.day_start_int
        return self.get_time_from_interval(first_day, last_day)
    last_month = property(get_last_month)
    last_month_str = property(lambda self: time2str(self.last_month))

    def get_this_week(self):
        today = datetime.date.today()
        first_day = int(datetime2time(today)) + self.owner.day_start_int - today.weekday() * 86400  # 86400 = day in seconds
        return self.get_time_from_interval(first_day, time.time())
    this_week = property(get_this_week)
    this_week_str = property(lambda self: time2str(self.this_week))

    def get_last_week(self):
        today = datetime.date.today()
        this_week_first_day = int(datetime2time(today)) + self.owner.day_start_int - today.weekday() * 86400  # 86400 = day in seconds
        last_week_first_day = this_week_first_day - 604800  # 604800 = week in seconds
        return self.get_time_from_interval(last_week_first_day, this_week_first_day)
    last_week = property(get_last_week)
    last_week_str = property(lambda self: time2str(self.last_week))


    def get_day_sum(self, date):
        start = int(datetime2time(date))
        stop = start + 86400  # 86400 = day in seconds
        return sum((obj.time for obj in self.timepart_set.active().filter(start__gte=start, start__lt=stop)))

    def get_day_sum_str(self, date):
        return time2str(self.get_day_sum(date))

    def __unicode__(self):
        return u"%s #%s" % (self.name, self.pk)


class TimePart(BaseModel):
    activity = models.ForeignKey(Activity)

    start = models.IntegerField(null=True, blank=True)
    stop = models.IntegerField(null=True, blank=True)

    note = models.CharField(max_length=255, default="", blank=True)

    def get_time(self):
        if self.stop:
            return self.stop - self.start
        return int(time.time()) - self.start
    time = property(get_time)

    def get_time_str(self):
        return time2str(self.get_time())
    time_str = property(get_time_str)

    def get_start_dt(self):
        return time2datetime(self.start)
    start_dt = property(get_start_dt)

    def get_stop_dt(self):
        if self.stop:
            return time2datetime(self.stop)
        return datetime.datetime.now()
    stop_dt = property(get_stop_dt)

    def __unicode__(self):
        return u"%s (%s - %s)" % (self.activity, self.start, self.stop)