from django.conf.urls.defaults import patterns, include, url
from django.views.generic.base import TemplateView

from django.contrib.auth.views import login as login_view, logout as logout_view

from .views import *

urlpatterns = patterns('',
    url(r'^$', index_view, name="index", ),
    url(r'^add-activity/$', add_activity_view, name="add_activity", ),
    url(r'^action/activity-(?P<activity_pk>\d+)/start/$', action_start_process, name="action_start", ),
    url(r'^action/activity-(?P<activity_pk>\d+)/stop/$', action_stop_process, name="action_stop", ),
    url(r'^action/activity-(?P<activity_pk>\d+)/delete/$', action_activity_delete_process, name="action_activity_delete", ),
    url(r'^action/time-part-(?P<time_part_pk>\d+)/delete/$', action_time_part_delete_process, name="action_time_part_delete", ),

    url(r'^activity-(?P<activity_pk>\d+)/delete/$', action_activity_delete_view, name="activity_delete", ),
    url(r'^time-part-(?P<time_part_pk>\d+)/delete/$', action_time_part_delete_view, name="time_part_delete", ),

    url(r'^export/activity-(?P<activity_pk>\d+).txt$', export_txt_view, name="export-txt", ),

    url(r'^activity-(?P<activity_pk>\d+)/detail/$', detail_view, name="detail", ),
    url(r'^activity-(?P<activity_pk>\d+)/settings/$', activity_settings_view, name="activity_settings", ),
    url(r'^activity-(?P<time_part_pk>\d+)/edit-note/$', edit_note_view, name="edit_note", ),

    url(r'^settings/$', settings_view, name="settings", ),
    url(r'^change-password/$', change_password_view, name="change_password", ),
    url(r'^change-password/done/$', TemplateView.as_view(template_name="tk/change_password_done.html"), name="change_password_done", ),

    url(r'^login/$', login_view, name="login", ),
    url(r'^logout/$', logout_view, name="logout", ),
)