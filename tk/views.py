import time
import datetime

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden, HttpResponseBadRequest
from django.template import RequestContext
from django.core.urlresolvers import reverse

from django.db.models import Q
from django.contrib.auth.decorators import login_required

from .models import Activity, TimePart
from .forms import ActivityForm, TimePartForm, LoginForm, SettingsForm
from .helpers import time2str, date2time


@login_required
def index_view(request, template="tk/index.html"):
    activities = Activity.objects.active().filter(Q(owner=request.user.profile) | Q(watchers=request.user.profile))

    today = datetime.datetime.now()
    this_month_first_day = datetime.date(day=1, month=today.month, year=today.year)
    last_month_last_day = this_month_first_day - datetime.timedelta(days=1)
    last_month_first_day = datetime.date(year=last_month_last_day.year, month=last_month_last_day.month, day=1)

    return render_to_response(template,
        {
            "Activity": Activity,

            "activities": activities,
            'last_month_start': int(date2time(last_month_first_day)),
            'last_month_stop': int(date2time(last_month_last_day)),
        },
        context_instance=RequestContext(request))

@login_required
def add_activity_view(request, template="tk/add_activity.html"):
    add_activity_form = ActivityForm(request.POST or None)

    if add_activity_form.is_valid():
        obj = add_activity_form.save(commit=False)
        obj.owner = request.user.profile
        obj.save()
        for watcher in add_activity_form.cleaned_data["watchers"]:
            obj.watchers.add(watcher)
        obj.save()
        return HttpResponseRedirect("/")

    return render_to_response(template,
        {
            "add_activity_form": add_activity_form,
        },
        context_instance=RequestContext(request))

@login_required
def activity_settings_view(request, activity_pk, template="tk/activity_settings.html"):
    activity = get_object_or_404(Activity, pk=activity_pk)
    if activity.owner != request.user.profile:
        return HttpResponseForbidden("403 Forbidden")

    activity_settings_form = ActivityForm(request.POST or None, instance=activity)

    if activity_settings_form.is_valid():
        activity_settings_form.save()
        return HttpResponseRedirect(reverse("tk:detail", args=(activity.pk, )))

    return render_to_response(template,
        {
            "activity": activity,

            "activity_settings_form": activity_settings_form,
        },
        context_instance=RequestContext(request))

@login_required
def action_start_process(request, activity_pk):
    activity = get_object_or_404(Activity, pk=activity_pk)
    if activity.owner != request.user.profile:
        return HttpResponseForbidden("403 Forbidden")


    if activity.timepart_set.all() and not activity.get_last_timepart().stop:
        return HttpResponseBadRequest("400 Already started")

    time_part = TimePart(activity=activity)
    time_part.start = time.time()
    time_part.save()
    return HttpResponseRedirect(request.GET.get("next", "/"))

@login_required
def action_stop_process(request, activity_pk):
    activity = get_object_or_404(Activity, pk=activity_pk)
    if activity.owner != request.user.profile:
        return HttpResponseForbidden("403 Forbidden")

    if not activity.timepart_set.all() and activity.get_last_timepart().stop:
        return HttpResponseBadRequest("400 Already stopped")

    time_part = activity.get_last_timepart()
    time_part.stop = time.time()
    time_part.save()
    return HttpResponseRedirect(request.GET.get("next", "/"))

@login_required
def action_activity_delete_view(request, activity_pk, template="tk/activity_delete.html"):
    activity = get_object_or_404(Activity, pk=activity_pk)
    if activity.owner != request.user.profile:
        return HttpResponseForbidden("403 Forbidden")

    return render_to_response(template,
    {
        "activity": activity,
    },
    context_instance=RequestContext(request))

@login_required
def action_time_part_delete_view(request, time_part_pk, template="tk/time_part_delete.html"):
    time_part = get_object_or_404(TimePart, pk=time_part_pk)
    if time_part.activity.owner != request.user.profile:
        return HttpResponseForbidden("403 Forbidden")

    return render_to_response(template,
    {
        "time_part": time_part,
    },
    context_instance=RequestContext(request))

@login_required
def action_activity_delete_process(request, activity_pk):
    activity = get_object_or_404(Activity, pk=activity_pk)
    if activity.owner != request.user.profile:
        return HttpResponseForbidden("403 Forbidden")

    activity.deleted = True
    activity.save()
    return HttpResponseRedirect(request.GET.get("next", "/"))

@login_required
def action_time_part_delete_process(request, time_part_pk):
    time_part = get_object_or_404(TimePart, pk=time_part_pk)
    if time_part.activity.owner != request.user.profile:
        return HttpResponseForbidden("403 Forbidden")

    time_part.deleted = True
    time_part.save()
    return HttpResponseRedirect(reverse("tk:detail", args=(time_part.activity.pk, )))

@login_required
def detail_view(request, activity_pk, template="tk/detail.html"):
    activity = get_object_or_404(Activity, pk=activity_pk)
    if activity.owner != request.user.profile and request.user.profile not in activity.watchers.active():
        return HttpResponseForbidden("403 Forbidden")

    return render_to_response(template,
        {
            "Activity": Activity,

            "activity": activity,
            "time_parts": activity.timepart_set.active(),
        },
        context_instance=RequestContext(request))

@login_required
def change_password_view(request, template="tk/change_password.html"):
    from django.contrib.auth.forms import PasswordChangeForm


    change_password_form = PasswordChangeForm(data=request.POST or None, user=request.user)
    if change_password_form.is_valid():
        change_password_form.save()
        return HttpResponseRedirect(reverse("tk:change_password_done"))
    return render_to_response(template,
        {
            "change_password_form": change_password_form,
        },
        context_instance=RequestContext(request))

@login_required
def edit_note_view(request, time_part_pk, template="tk/time_part.html"):
    time_part = get_object_or_404(TimePart, pk=time_part_pk)
    if time_part.activity.owner != request.user.profile:
        return HttpResponseForbidden("403 Forbidden")

    time_part_form = TimePartForm(request.POST or None, instance=time_part)
    time_part_form.fields["start_dt"].initial = datetime.datetime.fromtimestamp(time_part.start)
    if time_part.stop:
        time_part_form.fields["stop_dt"].initial = datetime.datetime.fromtimestamp(time_part.stop)
    if time_part_form.is_valid():
        obj = time_part_form.save(commit=False)
        obj.start = time.mktime(time_part_form.cleaned_data["start_dt"].timetuple())
        if obj.stop:
            if time_part_form.cleaned_data["stop_dt"]:
                obj.stop = time.mktime(time_part_form.cleaned_data["stop_dt"].timetuple())
            else:
                obj.stop = None
        obj.save()
        return HttpResponseRedirect(reverse("tk:detail", args=(time_part.activity.pk, )))

    return render_to_response(template,
        {
            "activity": time_part.activity,

            "time_part_form": time_part_form,
        },
        context_instance=RequestContext(request))

@login_required
def settings_view(request, template="tk/settings.html"):
    settings_form = SettingsForm(request.POST or None, instance=request.user.profile)
    if settings_form.is_valid():
        settings_form.save()
        return HttpResponseRedirect("")

    return render_to_response(template,
        {
            "settings_form": settings_form,
        },
        context_instance=RequestContext(request))

@login_required
def export_txt_view(request, activity_pk, template="tk/activity_export.txt"):
    activity = get_object_or_404(Activity, pk=activity_pk)
    if activity.owner != request.user.profile:
        return HttpResponseForbidden("403 Forbidden")

    try:
        start = int(request.GET.get('start'))
        stop = int(request.GET.get('stop'))

        if stop - start > 3600*24*366:
            return HttpResponseForbidden('403 Forbidden')
    except TypeError:
        return HttpResponseBadRequest('400 Bad Request')

    days = []
    days_sum = 0
    day = datetime.date.fromtimestamp(start)
    while True:
        print day
        day_sum = activity.get_day_sum(day)
        if day_sum:
            days.append({
                "day": day,
                "sum": time2str(day_sum),
            })
        days_sum += day_sum
        day = day + datetime.timedelta(days=1)
        if date2time(day) > stop:
             break
    return render_to_response(template,
    {
        "days": days,
        "activity": activity,
        "total": time2str(days_sum),
    },
    context_instance=RequestContext(request), content_type="text/plain")

